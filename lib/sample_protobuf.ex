defmodule Messages do
  @moduledoc """

  iex> msg = Messages.User.new(id: 1, name: "Darwin", email: "darwin@email.com", address: "Paranaque")
  %Messages.User{
    address: "Paranaque",
    email: "darwin@email.com",
    id: 1,
    name: "Darwin"
  }

  iex> encoded_msg = Messages.User.encode(msg)
  <<8, 1, 18, 6, 68, 97, 114, 119, 105, 110, 26, 16, 100, 97, 114, 119, 105, 110,
  64, 101, 109, 97, 105, 108, 46, 99, 111, 109, 34, 9, 80, 97, 114, 97, 110, 97,
  113, 117, 101>>

  iex> decoded_msg = Messages.User.decode(encoded_msg)
  %Messages.User{
    address: "Paranaque",
    email: "darwin@email.com",
    id: 1,
    name: "Darwin"
  }
  """
  use Protobuf, """

  message User {
    required int32 id = 1;
    required string name = 2;
    required string email = 3;
    optional string address = 4;
  }
  """
end
